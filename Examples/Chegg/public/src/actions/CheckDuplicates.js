/*
* @Author: mcavall
* @Date:   2016-10-19 22:09:54
* @Last Modified by:   mcavall
* @Last Modified time: 2016-10-20 09:54:33
*/

'use strict';

var updateCount = function(map, key) {

	//Already in hash map
	if(map[key]) {
		//Increase count
		map[key]++;
	}
	else {
		//Not found yet, add to hash map
		map[key] = 1;
	}

}


//Remove non duplicates. Could be also be removed by filtering results with count >1 in the view
var removeNonDuplicates = function(map) {
	Object.keys(map).forEach(function(c, i) {
		//If appears only one time
		if(map[c] == 1) delete map[c];
	});
}


//Checks for duplicates of single characters in an alphanumeric string
var CheckDuplicatesSimple = function(string) {

	//Note that we could potentially do this with a bit vector instead of a Javascript object, thus saving space complexity

	//Check validity of input (in our case not that much necessary)
	if(typeof string != 'string') return undefined;

	//We assume case doesn't matter
	string = string.toLowerCase();
	
	//Hash map to keep counts, with letters as keys
	var result = {};
	for (var i = 0; i < string.length; i++) {

		//Get character at ith position
		var c = string[i];
		updateCount(result, c);
		
	};

	removeNonDuplicates(result);

	return result;

}


//Checks for all possible duplicates in an alphanumeric string
var CheckDuplicatesAdvanced = function(string) {

	//Check validity of input (in our case not that much necessary)
	if(typeof string != 'string') return undefined;

	//We assume case doesn't matter
	string = string.toLowerCase();
	
	//Hash map to keep counts, with letters as keys
	var result = {};

	var predecessors = [];

	for (var i = 0; i < string.length; i++) {

		var c = string[i];

		predecessors.push('');

		predecessors.forEach(function(p, j) {
			predecessors[j] += c;
			updateCount(result, predecessors[j]);
		}); 

	}

	removeNonDuplicates(result);

	return result;
}


//Make the two functions public
module.exports = {
	Simple: CheckDuplicatesSimple,
	Advanced: CheckDuplicatesAdvanced
}