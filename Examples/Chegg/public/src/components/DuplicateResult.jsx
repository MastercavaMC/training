/*
* @Author: mcavall
* @Date:   2016-10-19 22:09:54
*/

var React = require('react');

var DuplicateResult = React.createClass({
	

	renderResult: function(key, i) {
		return (
			<div key={key} className="col-md-2 result">
				{key}
				<br />
				{this.props.result[key]}
			</div>
		);
	},


	render: function() {

		//Nothing to show
		if(this.props.result == undefined) return (null);

		//For each character in the results renders a block
		return (
			<div className="row">
				{Object.keys(this.props.result).map(this.renderResult)}
			</div>
		);
	}

});

module.exports = DuplicateResult;
