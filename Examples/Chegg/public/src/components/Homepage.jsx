/*
* @Author: mcavall
* @Date:   2016-10-19 22:09:54
*/

var React = require('react');

//Component with the requested functionality
var DuplicateStringChecker = require('./DuplicateStringChecker.jsx');

//Just a container to use as a home page
var Homepage = React.createClass({
	
	render: function() {
		return (
			<div className="row mt-lg">
				<div className="col-md-6 col-md-offset-3">

					<h1 className="mb-lg">Chegg.com Assignment</h1>

					<DuplicateStringChecker />

				</div>
			</div>
		);
	}

});

module.exports = Homepage;
