/*
* @Author: mcavall
* @Date:   2016-10-19 22:09:54
*/

var React = require('react');


//Imports the two requested functionalities
var CheckDuplicatesSimple = require('../actions/CheckDuplicates.js').Simple;
var CheckDuplicatesAdvanced = require('../actions/CheckDuplicates.js').Advanced;

//Component subview to show results
var DuplicateResult = require('./DuplicateResult.jsx');

var DuplicateStringChecker = React.createClass({


	//Mode one checks for single character duplication,
	//Mode two checks for all possible duplicates
	modes: {
		SIMPLE: 0,
		ADVANCED: 1, 
	},


	//Sets empty string and results
	getInitialState: function() {
		
		return {
			mode: this.modes.SIMPLE,
			string: '',
			result: undefined,
			validInput: undefined
		};

	},


	//Updates state with the input string
	setString: function(e) {
		
		var inputString = e.target.value;
		this.setState({string: inputString, validInput: this.checkAlphaNumeric(inputString)});

	},


	//Updates state with the check duplicates mode
	setMode: function(e) {
		
		var checkMode = e.target.value;
		this.setState({mode: checkMode});

	},


	//Checks if the input string is alphanumeric
	checkAlphaNumeric: function(string) {
		var re = /^[a-z0-9]+$/i;
		return re.test(string);
	},


	//Checks for duplication in the input string
	checkString: function() {

		var result;

		if(this.state.mode == this.modes.ADVANCED) {
			result = CheckDuplicatesAdvanced(this.state.string);
		} 
		else {
			result = CheckDuplicatesSimple(this.state.string);
		}

		console.log(result);
		this.setState({result: result});

	},


	//Renders a visual feedback for checking the input
	renderFeedback: function() {
		if(this.state.validInput != undefined) {
			return (
				<span className={"glyphicon glyphicon-" + (this.state.validInput ? "ok" : "remove" ) + " form-control-feedback"}></span>
			);
		}
	},


	//Renders the component html
	render: function() {
		return (
			<div>

				<div className="row">
					<div className={"col-md-7"}>
						<div className={"has-feedback " + (this.state.validInput != undefined ? (this.state.validInput ? "has-success" : "has-error") : "")}>
							<input type="text" value={this.state.string} onChange={this.setString} className="form-control" placeholder="Type or Paste a String" />
							{this.renderFeedback()}
						</div>
					</div>

					<div className="col-md-3">
						<select className="form-control" value={this.state.mode} onChange={this.setMode}>
							<option value={this.modes.SIMPLE}>Simple</option>
							<option value={this.modes.ADVANCED}>Advanced</option>
						</select>
					</div>

					<div className="col-md-2">
						<button className="btn btn-primary" onClick={this.checkString} disabled={this.state.validInput == undefined || !this.state.validInput} >Check</button>
					</div>

				</div>

				<h3>Duplicate String Checker</h3>

				<DuplicateResult result={this.state.result} />


			</div>
		);
	}


});

module.exports = DuplicateStringChecker;
