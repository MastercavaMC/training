/*
* @Author: mcavall
* @Date:   2016-10-19 22:09:54
*/


/** This file is used to create the bundle **/

var React = require('react');

//Using react-router for page routing
var Router = require('react-router').Router
var Route = require('react-router').Route
var ReactDOM = require('react-dom');


/** BROWSER HISTORY **/
var browserHistory = require('react-router').browserHistory;

/** REACT COMPONENTS **/
var Homepage = require("./components/Homepage.jsx");


/** ROUTING **/
ReactDOM.render( 
	<Router history={browserHistory}>
		<Route path="/" component={Homepage}></Route>
	</Router>
	, document.getElementById("content-div")
);


