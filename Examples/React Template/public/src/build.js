var Router = require('react-router').Router
var Route = require('react-router').Route
var IndexRoute = require('react-router').IndexRoute
var browserHistory = require('react-router').browserHistory;
var ReactDOM = require('react-dom');
var React = require('react');


/** PAGES **/
var Container = require('./components/Container.jsx');


/** ROUTING **/
//Add here additional routes
ReactDOM.render(
	<Router history={browserHistory} >
		<Route path="/" component= {Container}>

		</Route>
	</Router>
	, document.getElementById("content-div")
);

