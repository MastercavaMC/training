Created by Marco Cavallo
mcavall@us.ibm.com


To run the server:

npm start

or 

node server


To access the website:

go to http://localhost:4080/


To see components:

check public/src/components



To rebuild the bundle (you shuldn't need it):

npm run build


