/*
* @Author: mcavall
* @Date:   2016-10-19 22:09:54
* @Last Modified by:   mcavall
* @Last Modified time: 2017-01-03 02:29:15
*/

'use strict';

//Change it to your desired port, note that 80 requires root permission
const port = 4080;
const publicDirectory = __dirname + '/public/';

//Use express for file serving and url mapping
var express = require('express'), 
open = require('open'); 

var app = express(); 

//Make publicly accessible
app.use('/assets', express.static(publicDirectory + 'assets'));
app.use('/build', express.static(publicDirectory + 'build'));

//Request home page at root directory
app.get('/', function (req, res) {
  	res.sendFile(publicDirectory + "index.html");
});

console.log('Webserver started, waiting for incoming connections...');

//Start webserver
app.listen(port);

open('http://localhost:'+port); 


